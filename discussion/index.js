//alert('Hello World!');

/*
	Selection Control Structures
		- it sorts out wethere the statement/s are to be executed based on the condition whether it is true or false.
		- two-way selection (true or false)
		- multi-way selection
*/

// IF ELSE STATEMENTS

/*
	Syntax:
	if(condition) {
		statement
	} else if (condition) {
		statement
	} else {
		statement
	}
*/

/*
	if statement - Executes a statementif a specified condition is true
*/

let numA = -1;

if(numA < 0){
	console.log("Hello");
}
console.log(numA < 0);

let city = "New York";
if(city === "New York"){
	console.log("Welcome to New York City!");
};

/*
	Else if
		- executes a statement if our previous condiitons are false and if the specified condition is true.
		- the "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.
*/

let numB = 1;

if (numA > 0){
	console.log('Hello');
} else if (numB > 0){
	console.log('World');
};

city = "Tokyo";

if (city === "New York"){
	console.log('Welcome to New York City!');
} else if (city === 'Tokyo'){
	console.log('Welcome to Tokyo City!');
};

/*
	Else Statement
		- executes a statement if all of our previous conditions are false.
*/

if (numA > 0){
	console.log('Hello');
} else if (numB === 0){
	console.log('World');
} else {
	console.log("Again");
};

/*let age = parseInt(prompt("Enter your age:"));

if(age <= 18){
	console.log("You are not allowed to drink.");
} else{
	console.log("Congratulations! you can now take a shot!");
};*/

/*let height = parseInt(prompt("Enter your height please:"))

if(height <= 150){
	console.log("Did not pass the min requirement.")
} else{
	console.log("Passed the minimum height requirement!");
};
*/

// Nested if statement

let isLegalAge = true;
let isAdmin = false;

if(isLegalAge){
	if(!isAdmin){
		console.log("You are not an admin!");
	};
};


let message = "No message";

function determineTyphoonIntensity(windSpeed) {
    if (windSpeed < 30) {
        return 'Not a typhoon.';
    } else if (windSpeed <= 61) {
        return 'Tropical Depression Detected!';
    } else if (windSpeed >= 62 && windSpeed <= 88) {
        return 'Tropcial Storm Detected!';
    } else if (windSpeed >= 89 && windSpeed <= 177) {
        return 'Severe Tropical Storm Detected!'
    } else {
        return 'Typhoon detected.';
    };
};
message = determineTyphoonIntensity(70);
console.log(message);

message = determineTyphoonIntensity(178);
console.log(message);


// Truthy and Falsy

/*
	In JS a truthy value is value that is considered true when encounted in a boolean context.

	Falsy Values:
		1. false
		2. 0
		3 -0
		4. ""
		5. null
		6. underfined
		7. NaN
*/

// Truthy examples:

let word = "true";

if(word) {
	console.log("Truthy");
};

if(true) {
	console.log("Truthy");
};

if(10) {
	console.log("Truthy");
};

if ([]) {
	console.log("Truthy");
}


// Falsy examples:

if(false) {
	console.log("Falsy");
};

if(0) {
	console.log("Falsy");
};

if(-0) {
	console.log("Falsy");
}

if (undefined) {
	console.log("Falsy");
};

if (null) {
	console.log("Falsy");
};

if (NaN) {
	console.log("Falsy");
};

// Conditional (Ternary) Operator - for short codes

/*
	Ternary Operator takes in three operands
		1. condition
		2. expression to execute if the condition is true/truthy.
		3. expression to execute if the condition is false/falsy.

	Syntax:
		(condition) ? ifTrue_expression :
		ifFalse_expression
*/

// Single statement Execution
let ternaryResult= (1 < 18) ? "Condition is True" : "Condition is False";
console.log("Result of the Ternary Operator: " + ternaryResult);



let name;

function isOfLegalAge(){
	name = ' Louies';
	return 'You are of the legal age limit';
};

function isUnderAge(){
	name = ' Aronn';
	return 'You are under the age limit!'
};

let age = parseInt(prompt('What is your age?'));

let legalAge = (age > 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in Function: " + legalAge + ',' + name);


// Switch Statement

/*
	Can be used as an alternative to an if ... else statement where the data to be used in the condition of an expected input:

	Syntax:
	switch(expression) {
	case <value>;
		statement;
		break;
	default:
		statement;
		break;
	}
*/

let day = prompt("What day of the week is it today?").toLowerCase()
console.log(day);

switch(day) {
	case 'monday' && '1':
	console.log("The color of the day is red.");
		break;
	case 'tuesday' && '2':
	console.log("The color of the day is orange.");
		break;
	case 'wednesday' && '3':
	console.log("The color of the day is yellow.");
		break;
	case 'thursday' && '4':
	console.log("The color of the day is green.");
		break;
	case 'friday' && '5':
	console.log("The color of the day is blue.");
		break;
	case 'saturday' && '6':
	console.log("The color of the day is indigo.");
		break;
	case 'sunday' && '7':
	console.log("The color of the day is violet.");
		break;
	default:
		console.log("Please input a valid day!");
		break
}

// break; - is for ending the loop.
// switch - is not a very popular statenment in coding.

// Try-Catch-Finally Statement

/*
	- try-catch is commonly used for error handling.
	- will still function even if the statement is not complete 
*/

function showIntensityAlert(windSpeed) {
	try {
		alert(determineTyphoonIntensity(windSpeed));
	}
	catch (error) {
		console.log(typeof error);
		console.log(error);
		console.warn(error.message);
	}
	finally {
		alert("Intensity updates will show new alert!");
	}
}

showIntensityAlert(56);






















